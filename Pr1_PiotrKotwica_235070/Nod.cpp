#include "pch.h"
#include "Nod.h"






Nod::Nod(int ** matrix, int sizeOfMatrix, int nod)
{
	this->nod = nod;
	this->sizeOfMatrix = sizeOfMatrix;
	copyMatrix(matrix);
	this->parent = NULL;
	this->cost = reduceCost();
	this->level = 0;
	//this->bound = calculateBound(matrix, sizeOfMatrix);
	//displayCity(this->matrix, sizeOfMatrix);
}

void Nod::displayCity(int** miasto, int liczbaMiast) {

	if (miasto != NULL && liczbaMiast != 0) {
		cout << endl << endl;
		for (int i = 0; i < liczbaMiast; i++) {
			cout << "| ";

			for (int j = 0; j < liczbaMiast; j++) {
				cout << miasto[i][j];

				if (j != (liczbaMiast - 1))
					cout << "\t";
				else
					cout << " |\n";
			}
		}
		cout << "Koszt: " << this->getCost() << endl;
	}
	else
	{
		cout << endl << endl << "Nie wczytano pliku!";
	}
	system("pause");
}

int Nod::calculateBound(int** matrix, int size)
{
	int bound = 0;

	//licz dla przejsc
	Nod* tempParent = this->parent;
	int tempNod = this->nod;
	while (tempParent != NULL) {
		bound += matrix[tempParent->getNod()][nod];

		if (tempParent->getParent() != NULL)
			tempNod = tempParent->getNod();
			
		tempParent = tempParent->getParent();
	}

	//licz dal obecnego noda
	int min = INFINITY;
	for (int i = 0; i < size; i++) {
		if (this->isParent(i) && matrix[this->nod][i] < min)
			min = matrix[this->nod][i];
	}
	bound += min;

	//licz reszte
	min = INFINITY;
	for (int i = 0; i < size; i++) {
		if (this->isParent(i)) {
			for (int j = 0; j < size; j++) 
				if (this->isParent(j) && matrix[this->nod][i] < min)
					min = matrix[this->nod][i];
		}

		if(matrix[i][0] < min)
			min = matrix[this->nod][i];


		bound += min;
	}

	return bound;
}

Nod::Nod(int previousCost, Nod * parent, int nod)
{
	this->nod = nod;
	this->sizeOfMatrix = parent->getSizeOfMatrix();
	copyMatrix(parent->getMatrix());
	this->parent = parent;
	this->cost = previousCost + matrix[parent->getNod()][nod];
	fillInfinity();
	this->cost = this->cost + reduceCost();
	this->level = parent->getLevel() + 1;
	//displayCity(this->matrix, sizeOfMatrix);
}

Nod::Nod(int previousCost, Nod * parent, int nod, int** matrix, int size)
{
	this->nod = nod;
	this->sizeOfMatrix = parent->getSizeOfMatrix();
	copyMatrix(parent->getMatrix());
	this->parent = parent;
	this->cost = previousCost + matrix[parent->getNod()][nod];
	fillInfinity();
	this->cost = this->cost + reduceCost();
	this->level = parent->getLevel() + 1;
	//this->bound = calculateBound(matrix, size);
	//displayCity(this->matrix, sizeOfMatrix);
}

void Nod::copyMatrix(int** parentMatrix) 
{
	matrix = new int*[sizeOfMatrix] ;
	for (int i = 0; i < sizeOfMatrix; i++)
		matrix[i] = new int[sizeOfMatrix];

	for (int i = 0; i < sizeOfMatrix; i++)
		for (int j = 0; j < sizeOfMatrix; j++)
			matrix[i][j] = parentMatrix[i][j];
}

Nod::~Nod()
{
	if (matrix != NULL) {
		for (int i = 0; i < sizeOfMatrix; i++)
			delete[] matrix[i];
		delete[] matrix;
	}
}

void Nod::fillInfinity()
{
	for (int i = 0; i < sizeOfMatrix; i++) {
		matrix[this->parent->getNod()][i] = INFINITY;
		matrix[i][nod] = INFINITY;

		if (this->isParent(i))
			matrix[this->nod][i] = INFINITY;
	}
	//matrix[nod][parent->getNod()] = INFINITY;
}

int Nod::reduceCost()
{
	int reducedCost = 0;

	int minimumValue;



	for (int i = 0; i < sizeOfMatrix; i++) {				//szukanie minimalnej wartosci w wierszu

		minimumValue = INFINITY;

		for (int j = 0; j < sizeOfMatrix; j++) {
			if (i == j || matrix[i][j] == INFINITY)
				continue;

			if (matrix[i][j] < minimumValue)
				minimumValue = matrix[i][j];
		}

		if (minimumValue > 0 && minimumValue < INFINITY) {
			for (int j = 0; j < sizeOfMatrix; j++) {			//redukcja wiersza
				if (i == j || matrix[i][j] == INFINITY)
					continue;

				matrix[i][j] = matrix[i][j] - minimumValue;
			}

			reducedCost = reducedCost + minimumValue;
		}

	}


	for (int i = 0; i < sizeOfMatrix; i++) {					//szukanie minimalnej wartosci w kolumnie

		minimumValue = INFINITY;

		for (int j = 0; j < sizeOfMatrix; j++) {
			if (i == j || matrix[j][i] == INFINITY)
				continue;

			if (matrix[j][i] < minimumValue)
				minimumValue = matrix[j][i];

		}

		if (minimumValue > 0 && minimumValue < INFINITY) {
			for (int j = 0; j < sizeOfMatrix; j++) {			//redukcja kolumny
				if (i == j || matrix[j][i] == INFINITY)
					continue;

				matrix[j][i] = matrix[j][i] - minimumValue;
			}

			reducedCost = reducedCost + minimumValue;
		}

	}

	return reducedCost;
}

bool Nod::isParent(int nod)
{
	Nod* tempParent = this;

		while (tempParent != NULL) {
			if (tempParent->getNod() == nod)
				return true;

			tempParent = tempParent->getParent();
		}

		if (nod == this->nod)
			return true;

	
	return false;
}

 Nod* Nod::getParent() {
	 return parent;
}

int Nod::getLevel()
{
	return level;
}

int Nod::getNod()
{
	return nod;
}

int Nod::getCost()
{
	return cost;
}

int Nod::getBound()
{
	return bound;
}

int ** Nod::getMatrix()
{
	return matrix;
}

int Nod::getSizeOfMatrix()
{
	return sizeOfMatrix;
}

Stack * Nod::getParents()
{
	Stack* stack = new Stack();
	Nod* nxtParent = parent;

	stack->Push(nod);

	while(nxtParent != NULL){
		stack->Push(nxtParent->getNod());
		nxtParent = nxtParent->getParent();
	}

	return stack;
}
