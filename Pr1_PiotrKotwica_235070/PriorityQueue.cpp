#include "pch.h"
#include "PriorityQueue.h"


PriorityQueue::PriorityQueue()
{
	queue = NULL;
}


PriorityQueue::~PriorityQueue()
{
	while (queue != NULL)
	{
		Element* nextQueue = queue->nextElement;
		delete queue;
		queue = nextQueue;
	}
}


//sort by cost
void PriorityQueue::insert(Nod *newEdge)
{
	if (queue == NULL) {
		queue = new Element();
		queue->edge = newEdge;
		queue->nextElement = NULL;
	}
	else {
		Element* newElement = new Element();
		newElement->edge = newEdge;
		newElement->nextElement = NULL;

		Element* ptr = queue;
		while (ptr != NULL && ptr->edge->getCost() <= newElement->edge->getCost())
			ptr = ptr->nextElement;

		if (ptr == queue) {
			queue = newElement;
			newElement->nextElement = ptr;
		}
		else {
				Element* previousPtr = queue;
				while (previousPtr->nextElement != ptr)
					previousPtr = previousPtr->nextElement;

				newElement->nextElement = ptr;
				previousPtr->nextElement = newElement;
		}		
	}
}


//sort by bound
/*
void PriorityQueue::insert(Nod *newEdge)
{
	if (queue == NULL) {
		queue = new Element();
		queue->edge = newEdge;
		queue->nextElement = NULL;
	}
	else {
		Element* newElement = new Element();
		newElement->edge = newEdge;
		newElement->nextElement = NULL;

		Element* ptr = queue;
		while (ptr != NULL && ptr->edge->getBound() <= newElement->edge->getBound())
			ptr = ptr->nextElement;

		if (ptr == queue) {
			queue = newElement;
			newElement->nextElement = ptr;
		}
		else {
			Element* previousPtr = queue;
			while (previousPtr->nextElement != ptr)
				previousPtr = previousPtr->nextElement;

			newElement->nextElement = ptr;
			previousPtr->nextElement = newElement;
		}
	}
}
*/

Nod* PriorityQueue::remove()
{
	if (queue != NULL) {
		Nod* returnedEdge = queue->edge;

		Element* nextQueue = queue->nextElement;
		delete queue;
		queue = nextQueue;

		return returnedEdge;
	}

	return NULL;	//temporary solution
}


bool PriorityQueue::isEmpty() {
	if (queue == NULL)
		return true;
	else
		return false;
}
