#pragma once
#include <iostream>
#include <windows.h>
#include <iomanip>
#include "TravelingSalesmanBB.h"
#include "CityGenerator.h"

using namespace std;

class SprawozdanieMenu
{
	int **miasto;

public:
	SprawozdanieMenu();
	~SprawozdanieMenu();

	void menu();
	void testsBBWithDescribedAmountOfNods();
	bool CzyPoprawnaDecyzja(int max, int decyzja);
	long long int read_QPC();
};

