#pragma once
#include <windows.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include "TravelingSalesmanBB.h"
#include "Stack.h"

using namespace std;

class TestMenu
{
	int **miasto;
	int liczbaMiast;

public:
	TestMenu();
	~TestMenu();

	void resetCity();

	void menu();
	void wczytajZPliku(string file_name);
	bool file_read_line(ifstream & file, int tab[], int size);
	bool file_read_line(ifstream & file, int &edges);
	void displayCity();
	bool czyPoprawnaDecyzja(int max, int decyzja);
	long long int read_QPC();
	void znajdzNajkrotszaSciezke();
};

