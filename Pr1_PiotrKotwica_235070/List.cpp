#include "pch.h"
#include "List.h"


List::List()
{
	Head = NULL;
}


List::~List()
{
	Elem_List *nxt; //zapamietanie nastepnej struktury

	while (Head != NULL)	//usuwanie od poczatku struktur
	{
		nxt = Head->next;
		delete Head;
		Head = nxt;
	}
}

void List::Add(Nod* x)
{
	if (Head == NULL)
	{
		Head = new Elem_List;
		Head->key = x;
		Head->next = NULL;
	}
	else
	{
		Elem_List *newHead = new Elem_List;
		newHead->key = x;
		newHead->next = Head;

		Head = newHead;
		newHead = NULL;
	}
}