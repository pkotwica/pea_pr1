#pragma once
#include <iostream>
#include "Nod.h"
#include "PriorityQueue.h"
#include "List.h"
#include "Stack.h"


using namespace std;

class TravelingSalesmanBB
{
	int** matrix;
	int size;

private:
	void prepareMatrix();
	void addChildren(Nod * v, Nod * &tempNod, PriorityQueue * queue, List * list, bool &ifEnd);

public:
	TravelingSalesmanBB(int** matrix, int size);
	~TravelingSalesmanBB();

	Stack* searchSolution();
};

