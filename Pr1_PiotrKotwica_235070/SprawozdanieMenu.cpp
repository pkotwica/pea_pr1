#include "pch.h"
#include "SprawozdanieMenu.h"


SprawozdanieMenu::SprawozdanieMenu()
{
}


SprawozdanieMenu::~SprawozdanieMenu()
{
}

void SprawozdanieMenu::menu()
{
	int decyzja;
	bool czyKoniec = false;

	while (!czyKoniec)
	{
		system("cls");
		cout << "PROJEKT 1 - PROBLEM KOMIWOJAZERA (TSP)" << endl;
		cout << "MENU DO SPRAWOZDANIA" << endl;
		cout << "______________________________________" << endl << endl;

		cout << "Wybierz opcje" << endl;
		cout << "1 - Rozpocznij testy z podawaniem ilosci wierzcholkow B&B" << endl;
		cout << "2 - Do sprawozdania B&B" << endl;
		cout << "3 - WYJSCIE" << endl;
		cout << "Podaj numer: ";
		cin >> decyzja;

		do
		{
			if (CzyPoprawnaDecyzja(3, decyzja))
				break;

			cout << "Zly numer!" << endl;
			cout << "Podaj numer: ";
			cin >> decyzja;

		} while (!CzyPoprawnaDecyzja(4, decyzja));


		switch (decyzja)
		{
		case 1:
			testsBBWithDescribedAmountOfNods();
			break;
		case 2:
			//sprawozdanieMenu->menu();
			break;
		case 3:
			czyKoniec = true;
			break;
		}
	}
}

void SprawozdanieMenu::testsBBWithDescribedAmountOfNods()
{
	long long int frequency, start;
	long long int elapsed = 0;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);
	
	int amountOfNods;
	float amountOfIterations;
	long long int sumOftime = 0;
	int** matrix;
	CityGenerator* cityGenerator = new CityGenerator();
	TravelingSalesmanBB* tspAlgorithm;

	cout << "\nPodaj ilosc wirzcholkow do testowania: ";
	cin >> amountOfNods;
	cout << "\nPodaj ilosc iteracji: ";
	cin >> amountOfIterations;


	for (int i = 0; i < amountOfIterations; i++) {
		matrix = cityGenerator->generate(amountOfNods);
		tspAlgorithm = new TravelingSalesmanBB(matrix, amountOfNods);

		start = read_QPC();
		tspAlgorithm->searchSolution();
		elapsed = elapsed + read_QPC() - start;

		for (int j = 0; j < amountOfNods; j++)
			delete[] matrix[j];
		delete[] matrix;

		delete tspAlgorithm;
	}

	delete cityGenerator;
	
	float time = (float)elapsed / amountOfIterations;
	cout << "Srednie czasy wykonanych pomiarow: ";
	cout << "\tTime [s] = " << fixed << setprecision(3) << time /
		frequency << endl;
	cout << "\tTime [ms] = " << setprecision(0) << (1000.0 * time) /
		frequency << endl;
	cout << "\tTime [us] = " << setprecision(0) << (1000000.0 * time) /
		frequency << endl << endl;
	system("PAUSE");
	system("PAUSE");
	system("PAUSE");

}


bool SprawozdanieMenu::CzyPoprawnaDecyzja(int max, int decyzja)
{
	if (decyzja > max || decyzja < 1)
		return false;
	else
		return true;
}


long long int SprawozdanieMenu::read_QPC()
{
	LARGE_INTEGER count;
	QueryPerformanceCounter(&count);
	return((long long int)count.QuadPart);
}
