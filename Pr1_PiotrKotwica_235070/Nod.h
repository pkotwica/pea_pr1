#pragma once
#include <iostream>
#include "Stack.h"
#define INFINITY 999999


class Nod
{
	int nod;
	int** matrix;
	int sizeOfMatrix;
	int cost;
	int bound;
	Nod* parent;
	int level;

private:
	void fillInfinity();
	int reduceCost();
	void copyMatrix(int ** parentMatrix);
	void displayCity(int** miasto, int liczbaMiast);
	int calculateBound(int** matrix, int size);

public:
	//Nod();
	Nod(int** matrix, int sizeOfMatrix, int nod);	//dla roota
	Nod(int previousCost, Nod* parent, int nod);	//wlasciwe
	Nod(int previousCost, Nod* parent, int nod, int** matrix, int size);
	~Nod();


	bool isParent(int nod);
	Nod * getParent();
	int getLevel();
	int getNod();
	int getCost();
	int getBound();
	int** getMatrix();
	int getSizeOfMatrix();
	Stack* getParents();
};

