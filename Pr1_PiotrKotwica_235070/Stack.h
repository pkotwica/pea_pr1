#pragma once
#include <iostream>

using namespace std;

class Stack
{
	struct StackElem{
		int key = 0;
		StackElem* next = NULL;
	};

	StackElem* head;

public:
	Stack();
	~Stack();

	void Push(int x);
	int Pop();
	bool isEpmty();
};

