#include "pch.h"
#include "TestMenu.h"


TestMenu::TestMenu()
{
	miasto = NULL;
	liczbaMiast = 0;
}


TestMenu::~TestMenu()
{
	resetCity();
}

void TestMenu::resetCity()
{
	if (liczbaMiast != 0 && miasto != NULL)
	{
		for (int i = 0; i < liczbaMiast; i++)
			delete[] miasto[i]; //uwolnienie pamieci
		delete[] miasto; //uwolnienie pamieci

		miasto = NULL;
		liczbaMiast = 0;
	}
}


void TestMenu::menu()
{
	int decyzja;
	bool czyKoniec = false;
	string filename;

	while (!czyKoniec)
	{
		system("cls");
		cout << "PROJEKT 1 - PROBLEM KOMIWOJAZERA (TSP)" << endl;
		cout << "MENU DO TESTOWANIA" << endl;
		cout << "______________________________________" << endl << endl;

		cout << "Wybierz opcje" << endl;
		cout << "1 - Wczytaj graf z pliku" << endl;
		cout << "2 - Wyswietl wczytany plik" << endl;
		cout << "3 - Rozwiaz TSP - B&B" << endl;
		cout << "4 - WYJSCIE" << endl;
		cout << "Podaj numer: ";
		cin >> decyzja;

		do
		{
			if (czyPoprawnaDecyzja(4, decyzja))
				break;

			cout << "Zly numer!" << endl;
			cout << "Podaj numer: ";
			cin >> decyzja;

		} while (!czyPoprawnaDecyzja(4, decyzja));


		switch (decyzja)
		{
		case 1:
			cout << "Podaj nazwe pliku: ";
			cin >> filename;
			wczytajZPliku(filename);
			cout << "\nPlik zostal wczytany.\n";
			system("pause");
			break;
		case 2:
			displayCity();
			break;
		case 3:
			znajdzNajkrotszaSciezke();
			break;
		case 4:
			czyKoniec = true;
			break;
		}
	}
}


void TestMenu::znajdzNajkrotszaSciezke()
{
	long long int frequency, start, elapsed;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	TravelingSalesmanBB *travelingSalesmanBB = new TravelingSalesmanBB(miasto, liczbaMiast);
	int przebytaOdleglosc = 0;
	int zMiasta, doMiasta;

	start = read_QPC();
	Stack *stack = travelingSalesmanBB->searchSolution();
	elapsed = read_QPC() - start;

	cout << "Droga: ";

	if (!stack->isEpmty()) {
		zMiasta = stack->Pop();
		cout << zMiasta;
	}
	
	for (int i = 1; i < liczbaMiast; i++) {
		if (!stack->isEpmty()) {
			doMiasta = stack->Pop();
			cout << " -> " << doMiasta;
			przebytaOdleglosc = przebytaOdleglosc + miasto[zMiasta][doMiasta];
			zMiasta = doMiasta;
		}
	}
	
	cout << " -> 0\n";
	przebytaOdleglosc = przebytaOdleglosc + miasto[zMiasta][0];
	cout << "Dlugosc Drogi: " << przebytaOdleglosc << "\n";
	cout << "Czas algorytmu: \n";
	cout << "\tTime [s] = " << fixed << setprecision(3) << (float)elapsed /
		frequency << endl;
	cout << "\tTime [ms] = " << setprecision(0) << (1000.0 * elapsed) /
		frequency << endl;
	cout << "\tTime [us] = " << setprecision(0) << (1000000.0 * elapsed) /
		frequency << endl << endl;
	system("PAUSE");
}


void TestMenu::wczytajZPliku(string file_name)
{
	ifstream file;
	file.open(file_name.c_str());

	if (file.is_open())
	{
		resetCity();
		if (file_read_line(file, liczbaMiast))
		{
			miasto = new int *[liczbaMiast];			//tworzenie odpowiednio duzej macierzy
			for (int i = 0; i < liczbaMiast; i++)
				miasto[i] = new int[liczbaMiast];

			int *tab = new int [liczbaMiast];

			for (int i = 0; i < liczbaMiast; i++)
				if (file_read_line(file, tab, liczbaMiast))
				{
					for (int j = 0; j < liczbaMiast; j++)
						miasto[i][j] = tab[j];
				}
				else
				{
					cout << "File error - READ EDGE" << endl;
					break;
				}

			delete[] tab;
		}
		else
			cout << "File error - READ INFO" << endl;
		file.close();
	}
	else
		cout << "File error - OPEN" << endl;

}


bool TestMenu::file_read_line(ifstream &file, int tab[], int size)
{
	string s;
	getline(file, s);

	if (file.fail() || s.empty())
		return(false);

	istringstream in_ss(s);

	for (int i = 0; i < size; i++)
	{
		in_ss >> tab[i];
		if (in_ss.fail())
			return(false);
	}
	return(true);
}


bool TestMenu::file_read_line(ifstream &file, int &edges)
{
	string s;
	getline(file, s);

	if (file.fail() || s.empty())
		return(false);

	istringstream in_ss(s);

	in_ss >> edges;
	if (in_ss.fail())
		return(false);

	return(true);
}



void TestMenu::displayCity() {
	
	if (miasto != NULL && liczbaMiast != 0) {
		cout << endl << endl;
		for (int i = 0; i < liczbaMiast; i++) {
			cout << "| ";

			for (int j = 0; j < liczbaMiast; j++) {
				cout << miasto[i][j];

				if (j != (liczbaMiast - 1))
					cout << "\t";
				else
					cout << " |\n";
			}
		}
	}
	else
	{
		cout << endl << endl << "Nie wczytano pliku!";
	}
	system("pause");
}




bool TestMenu::czyPoprawnaDecyzja(int max, int decyzja)
{
	if (decyzja > max || decyzja < 1)
		return false;
	else
		return true;
}


long long int TestMenu::read_QPC()
{
	LARGE_INTEGER count;
	QueryPerformanceCounter(&count);
	return((long long int)count.QuadPart);
}