#include "pch.h"
#include "Stack.h"


Stack::Stack()
{
	head = NULL;
}


Stack::~Stack()
{
	StackElem *nxt; //zapamietanie nastepnej struktury

	while (head != NULL)	//usuwanie od poczatku struktur
	{
		nxt = head->next;
		delete head;
		head = nxt;
	}
}

void Stack::Push(int x)
{
	if (head == NULL)
	{
		head = new StackElem();
		head->key = x;
		head->next = NULL;
	}
	else
	{
		StackElem* newHead = new StackElem();
		newHead->key = x;
		newHead->next = head;

		head = newHead;
		newHead = NULL;
	}
}

int Stack::Pop()
{
	if (head != NULL) {
		StackElem* popElem = head;
		int popKey = popElem->key;

		head = head->next;
		delete popElem;

		return popKey;
	}

	return 0;
}

bool Stack::isEpmty()
{
	if (head == NULL)
		return true;
	else
		return false;
}
