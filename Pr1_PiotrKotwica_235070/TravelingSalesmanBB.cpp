#include "pch.h"
#include "TravelingSalesmanBB.h"


void TravelingSalesmanBB::prepareMatrix()
{
	for (int i = 0; i < size; i++)
		matrix[i][i] = INFINITY;
}

TravelingSalesmanBB::TravelingSalesmanBB(int** matrix, int size)
{
	this->matrix = matrix;
	this->size = size;
}


TravelingSalesmanBB::~TravelingSalesmanBB()
{
}

/*
Stack * TravelingSalesmanBB::searchSolution()
{
	prepareMatrix();
	PriorityQueue* queue = new PriorityQueue();
	Nod *u = NULL;
	Nod* v = new Nod(matrix, size, 0);
	int best = v->getBound();
	Nod* bestNod = v;

	queue->insert(v);

	while (!queue->isEmpty()) {
		v = queue->remove();

		if (v->getBound() <= best) {
			for (int i = 1; i < size; i++) {
				if (!v->isParent(i)) {
					u = new Nod(v->getCost(), v, i);

					if (u->getCost() <= best) {
						best = u->getBound();
						bestNod = u;
					}

					if (u->getBound() <= best)
						queue->insert(u);
				}
			}
		}

	}

	
	Stack* solution = bestNod->getParents();


	delete queue;

	return solution;
}
*/


Stack * TravelingSalesmanBB::searchSolution()
{
	prepareMatrix();
	PriorityQueue* queue = new PriorityQueue();
	List* list = new List();		//lista do przechowywania utworzonych nodow aby je potem usunac z pamieci (delete)

	Nod* v = NULL;
	Nod* bestNod = NULL;
	Nod* tempNod = new Nod(matrix, size, 0);
	queue->insert(tempNod);
	list->Add(tempNod);

	bool ifEnd = false;

	//while (!queue->isEmpty() && !ifEnd) {
	while (!queue->isEmpty()) {

		v = queue->remove();
		ifEnd = true;

		if (bestNod == NULL)
			addChildren(v, tempNod, queue, list, ifEnd);
		else
			if (bestNod->getCost() > v->getCost())
				addChildren(v, tempNod, queue, list, ifEnd);			//dodawnia potomkow

		if (ifEnd) {
			if (bestNod == NULL)
				bestNod = v;										//zapamietanie liscia / jezeli lisc juz jakis byl to wyznaczanie czy jest lepszy od poprzedniego 
			else
				if (bestNod->getCost() > v->getCost())
					bestNod = v;

			//tempNod = queue->remove();							//sciaganie z kolejki nastepnego noda zeby sprawdzic czy moze jest jakas lepsza droga


			//if (tempNod->getCost() < bestNod->getCost()) {
			//	ifEnd = false;
			//	queue->insert(tempNod);
			//	queue->insert(bestNod);
			//}
		}
	}



	Stack* solution = bestNod->getParents();


	delete queue;
	delete list;

	return solution;
}



void TravelingSalesmanBB::addChildren(Nod * v, Nod * &tempNod, PriorityQueue * queue, List * list, bool &ifEnd)
{
	for (int i = 1; i < size; i++) {
		if (!v->isParent(i)) {
			tempNod = new Nod(v->getCost(), v, i);			//dodawanie potomkow
			queue->insert(tempNod);
			list->Add(tempNod);
			ifEnd = false;
		}
	}
}

