#pragma once
#include<iostream>
#include <random>

using namespace std;

static class CityGenerator
{
	void resetCity(int** city,int amountOfCity);
	int random();

public:
	CityGenerator();
	~CityGenerator();
	
	int** generate(int amountOfCity);
	void display(int** city, int amount);
};

