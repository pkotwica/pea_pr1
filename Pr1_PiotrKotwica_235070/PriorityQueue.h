#pragma once
#include <iostream>
#include "Nod.h"

using namespace std;

class PriorityQueue
{
	struct Element {
		Nod* edge = NULL;
		Element* nextElement = NULL;
	};

	Element* queue;

public:
	PriorityQueue();
	~PriorityQueue();

	 void insert(Nod *newEdge);
	 Nod* remove();
	 bool isEmpty();
};

