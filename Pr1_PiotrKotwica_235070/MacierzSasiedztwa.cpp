#include "pch.h"
#include "MacierzSasiedztwa.h"


Macierz_Sasiedztwa::Macierz_Sasiedztwa()
{
	macierz = NULL;
	liczba_wierzcholkow = 0;
	czy_skierowany = true;
}

Macierz_Sasiedztwa::Macierz_Sasiedztwa(int wielkosc, bool skierowany)
{
	liczba_wierzcholkow = wielkosc;
	czy_skierowany = skierowany;

	macierz = new int *[liczba_wierzcholkow];

	for (int i = 0; i < liczba_wierzcholkow; i++)
	{
		macierz[i] = new int[liczba_wierzcholkow];

		for (int j = 0; j < liczba_wierzcholkow; j++)
			macierz[i][j] = NIESKONCZONOSC;
	}
}


Macierz_Sasiedztwa::~Macierz_Sasiedztwa()
{
	if (macierz != NULL && liczba_wierzcholkow > 0)
	{
		for (int i = 0; i < liczba_wierzcholkow; i++)
			delete[] macierz[i]; //uwolnienie pamieci
		delete[] macierz; //uwolnienie pamieci
	}

	macierz = NULL;
	liczba_wierzcholkow = 0;
}





void Macierz_Sasiedztwa::Dodawanie_krawedzi(int wierzcholek_pocz, int wierzcholek_kon, int waga)
{
	if (czy_skierowany == true)
	{
		macierz[wierzcholek_pocz][wierzcholek_kon] = waga;
	}
	else
	{
		macierz[wierzcholek_pocz][wierzcholek_kon] = waga;
		macierz[wierzcholek_kon][wierzcholek_pocz] = waga;
	}
}






void Macierz_Sasiedztwa::Display()
{
	if (macierz != NULL)
	{
		int suma_wag = 0;
		cout << "Wdge\t" << "Weight\n";

		for (int i = 0; i < liczba_wierzcholkow; i++)
		{
			for (int j = 0; j < liczba_wierzcholkow; j++)
			{
				if (macierz[i][j] != NIESKONCZONOSC)
				{
					cout << "(" << i << ", " << j << ")\t" << macierz[i][j] << endl;
					suma_wag = suma_wag + macierz[i][j];
				}
			}
		}

		cout << "Waga = " << suma_wag << endl;
	}
	else
	{
		cout << "Brak zainicjowanej macierzy!\n";
	}
}



void Macierz_Sasiedztwa::Display_macierz()
{
	if (macierz != NULL)
	{
		cout << "    ";

		for (int i = 0; i < liczba_wierzcholkow; i++)
		{

			cout.width(5);
			cout << right << i;
		}

		cout << endl;

		for (int i = 0; i < liczba_wierzcholkow; i++)
		{
			cout << " " << i << " |";
			for (int j = 0; j < liczba_wierzcholkow; j++)
			{
				if (macierz[i][j] != NIESKONCZONOSC)
				{
					cout.width(5);
					cout << internal << macierz[i][j];
				}
				else
				{
					cout.width(5);
					cout << internal << "x";
				}
			}
			cout << "|" << endl;
		}
		cout.clear();
	}
	else
	{
		cout << "Brak zainicjowanej macierzy!\n";
	}
}





bool Macierz_Sasiedztwa::Search(int pocz, int kon)
{
	if (macierz[pocz][kon] != NIESKONCZONOSC)
		return true;
	else
		return false;
}
