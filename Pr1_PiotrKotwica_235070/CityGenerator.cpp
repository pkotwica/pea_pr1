#include "pch.h"
#include "CityGenerator.h"


CityGenerator::CityGenerator()
{
}


CityGenerator::~CityGenerator()
{
}

void CityGenerator::resetCity(int** city, int amountOfCity) {
	if (amountOfCity != 0 && city != NULL)
	{
		for (int i = 0; i < amountOfCity; i++)
			delete[] city[i]; //uwolnienie pamieci
		delete[] city; //uwolnienie pamieci

		city = NULL;
		amountOfCity = 0;
	}
}

int** CityGenerator::generate(int amountOfCity)
{
	int** city;

	city = new int *[amountOfCity];
	for (int i = 0; i < amountOfCity; i++)
		city[i] = new int[amountOfCity];


	for (int i = 0; i < amountOfCity; i++) {
		for (int j = 0; j < amountOfCity; j++) {
			if (i == j)
				city[i][j] = 0;
			else
				city[i][j] = random();
		}
	}
	//this->display(city, amountOfCity);

	return city;
}

void CityGenerator::display(int ** city, int amount)
{
	if (city != NULL && amount != 0) {
		cout << endl << endl;
		for (int i = 0; i < amount; i++) {
			cout << "| ";

			for (int j = 0; j < amount; j++) {
				cout << city[i][j];

				if (j != (amount - 1))
					cout << "\t";
				else
					cout << " |\n";
			}
		}
	}
	else
	{
		cout << endl << endl << "Nie wczytano pliku!";
	}
	system("pause");
}



int CityGenerator::random()
{
	random_device rd; // non-deterministic generator
	mt19937 gen(rd()); // random engine seeded with rd()
	uniform_int_distribution<> dist(1, 100); // distribute results between
	 // 1 and 1000000 inclusive
	
	return dist(gen);
}
