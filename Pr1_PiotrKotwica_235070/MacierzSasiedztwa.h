#pragma once
#include <iostream>
#define NIESKONCZONOSC 999999

using namespace std;


class Macierz_Sasiedztwa
{

	int **macierz;
	int liczba_wierzcholkow;
	bool czy_skierowany;

public:
	Macierz_Sasiedztwa();
	Macierz_Sasiedztwa(int, bool);
	~Macierz_Sasiedztwa();


	//void Tworzenie_macierzy(int wielkosc, bool skierowany);
	void Dodawanie_krawedzi(int wierzcholek_pocz, int wierzcholek_kon, int waga);


	void Display();
	void Display_macierz();

	bool Search(int, int);
};
