#pragma once
#include <iostream>
#include "Nod.h"

using namespace std;

class List
{
	struct Elem_List
	{
		Nod* key;
		Elem_List *next = NULL;
	};

	Elem_List *Head;

public:
	List();
	~List();

	void Add(Nod* x);
};

